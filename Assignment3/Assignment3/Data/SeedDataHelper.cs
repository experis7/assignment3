﻿using Assignment3.Model.Domain;

namespace Assignment3.Data
{
    public class SeedDataHelper
    {
        public static List<Movie> GetMovies()
        {
            List<Movie> movies = new List<Movie>
            {
                new Movie() {Id = 1,Title= " The Godfather", Genre ="Crime Drama", ReleaseYear = "1972", Director = "Francis Ford Coppola", Picture = "https://www.imdb.com/title/tt0068646/mediaviewer/rm746868224/?ref_=tt_ov_i",Trailer = "https://www.imdb.com/video/vi1348706585/?playlistId=tt0068646&ref_=tt_ov_vi", FranchiseId = 1},
                new Movie() {Id = 2,Title= "The Dark Knight", Genre= "Action Crime Drama Thriller", ReleaseYear = "2008", Director = "Christopher Nolan", Picture = "https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i", Trailer= "https://www.imdb.com/video/vi324468761/?playlistId=tt0468569&ref_=tt_ov_vi",FranchiseId = 1},
                new Movie() {Id = 3, Title = "The Lord of the Rings: The Return of the King", Genre = "Action Adventure Drama Fantasy", ReleaseYear = "2003", Director = "Peter Jackson", Picture = "https://www.imdb.com/title/tt0167260/mediaviewer/rm584928512/?ref_=tt_ov_i", Trailer = "https://www.imdb.com/video/vi718127897/?playlistId=tt0167260&ref_=tt_ov_vi", FranchiseId = 1},
                new Movie() {Id = 4, Title = "Schindler's List", Genre = "Biography Drama History", ReleaseYear = "1993", Director = "Steven Spielberg", Picture = "https://www.imdb.com/title/tt0108052/mediaviewer/rm1610023168/?ref_=tt_ov_i", Trailer = "https://www.imdb.com/video/vi1158527769/?playlistId=tt0108052&ref_=tt_ov_vi", FranchiseId = 1}
            };
            return movies;
        }

        public static List<Character> GetCharacters()
        {
            List<Character> characters = new List<Character>
            {
                new Character{Id = 1,FullName = "Oskar Schindler", Gender = "Male", },
                new Character{Id = 2,FullName = "Don Vito Corleone", Gender = "Male"},
                new Character{Id = 3,FullName = "Bruce Wayne", Alias = "Batman", Gender = "Male"},
                new Character{Id = 4,FullName = "Samvis Gamgod", Alias= "Sam" , Gender = "Male"}


            };
            return characters;
        }

        public static List<Franchise> GetFranchises()
        {
            List<Franchise> franchises = new List<Franchise>
            {
                new Franchise(){Id = 1,Name= "The Lord of the Rings", Description = "The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel The Lord of the Rings by J. R. R. Tolkien."}
            };
            return franchises;
        }
    }
}
