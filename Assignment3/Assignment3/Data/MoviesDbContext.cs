﻿using Assignment3.Model.Domain;
using Microsoft.EntityFrameworkCore;

namespace Assignment3.Data
{
    public class MoviesDbContext: DbContext
    {
        public MoviesDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Character> Characters { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().HasData
                (
                SeedDataHelper.GetMovies()
                );
            modelBuilder.Entity<Movie>()
                .HasMany(x => x.Characters)
                .WithMany(p => p.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacter",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    jt =>
                    {
                        jt.HasKey("CharacterId", "MovieId");
                        jt.HasData(
                            new {CharacterId = 1,MovieId = 4},
                            new {CharacterId = 2, MovieId = 1},
                            new {CharacterId = 3, MovieId = 2},
                            new {CharacterId = 4, MovieId = 3}

                                  );
                    });
          


            modelBuilder.Entity<Character>().HasData
                (
                SeedDataHelper.GetCharacters()
                );


            modelBuilder.Entity<Franchise>()
                .HasData(
                SeedDataHelper.GetFranchises()
                );
        }
    }
}
