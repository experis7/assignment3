﻿using Assignment3.Model.Domain;
using Assignment3.Model.DTO.Franchise;
using AutoMapper;

namespace Assignment3.Profiles
{
    public class FranchiseProfile: Profile
    {
        public FranchiseProfile()
        {
            CreateMap<FranchiseCreateDTO,Franchise>();
            CreateMap<FranchiseEditDTO, Franchise>();
            CreateMap<Franchise, FranchiseReadDTO>();

            
             

        }
    }
}
