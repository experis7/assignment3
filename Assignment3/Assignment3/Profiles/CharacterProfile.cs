﻿using Assignment3.Model.Domain;
using Assignment3.Model.DTO.Character;
using AutoMapper;

namespace Assignment3.Profiles
{
    public class CharacterProfile:Profile
    {
        public CharacterProfile()
        {
            CreateMap<CharacterCreateDTO,Character >();
            CreateMap<CharacterEditDTO, Character>();
            CreateMap<Character,CharacterReadDTO>();
        }
    }
}
