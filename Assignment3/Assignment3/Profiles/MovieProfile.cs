﻿using Assignment3.Model.Domain;
using Assignment3.Model.DTO.Movie;
using AutoMapper;

namespace Assignment3.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>();
            CreateMap<MovieCreateDTO, Movie>();
            CreateMap<MovieEditDTO, Movie>();
           
            
        }
    }
}
