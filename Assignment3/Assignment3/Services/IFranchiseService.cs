﻿using Assignment3.Model.Domain;

namespace Assignment3.Services
{
    public interface IFranchiseService
    {
        public Task<Franchise> GetSpesificFranchiseAsync(int id);
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);
        public  Task AddMovieToFranchiseAsync(int id, List<int> movieIds);
        public bool FranchiseExists(int id);
        public  Task<List<Movie>> GetMoviesInFranchiseAsync(int id);
        public  Task<List<Character>> GetCharactersInFranchiseAsync(int id);
        public  Task DeleteFranchiseAsync(int id);
        public Task UpdateFranchiseAsync(Franchise franchise);
    }
}
