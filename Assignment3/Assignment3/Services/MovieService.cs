﻿using Assignment3.Data;
using Assignment3.Model.Domain;
using Assignment3.Model.DTO.Character;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Assignment3.Services
{
    public class MovieService : IMovieService
    {
        private readonly MoviesDbContext _context;

        public MovieService(MoviesDbContext context)
        {
            _context = context;
        }

        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

        }

        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies.ToListAsync();
        }

        public async Task<Movie> GetSpecificMovieAsync(int id)
        {
            return await _context.Movies.FindAsync(id);
        }

        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }

        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();

        }
        public async Task PutCharactersInMovieAsync(int id, List<int> characterIds)
        {
            Movie movieToUpdateCharacters = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.Id == id)
                .FirstAsync();
            List<Character> characters = new List<Character>();
            foreach (int charId in characterIds)
            {
                Character character = await _context.Characters.FindAsync(charId);
                if (character == null)
                    throw new KeyNotFoundException();
                characters.Add(character);
            }
            movieToUpdateCharacters.Characters = characters;
            await _context.SaveChangesAsync();
        }

        public async Task<List<Character>> GetCharactersInMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            return await _context.Characters.Where(c => c.Movies.Contains(movie)).ToListAsync();
            
            
        }
    }
}
