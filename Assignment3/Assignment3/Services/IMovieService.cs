﻿using Assignment3.Model.Domain;

namespace Assignment3.Services
{
    public interface IMovieService
    {
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        public Task<Movie> GetSpecificMovieAsync(int id);
        public Task<Movie> AddMovieAsync(Movie movie);
        public Task UpdateMovieAsync(Movie movie);
        public Task DeleteMovieAsync(int id);
        public bool MovieExists(int id);
        public Task PutCharactersInMovieAsync(int id, List<int> characterIds);
        public  Task<List<Character>> GetCharactersInMovieAsync(int id);
    }
}
