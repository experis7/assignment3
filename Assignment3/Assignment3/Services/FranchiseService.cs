﻿using Assignment3.Data;
using Assignment3.Model.Domain;
using Microsoft.EntityFrameworkCore;

namespace Assignment3.Services
{
    public class FranchiseService:IFranchiseService
    {
        private readonly MoviesDbContext _context;

        public FranchiseService(MoviesDbContext context)
        {
            _context = context;
        }

        public async Task<Franchise> GetSpesificFranchiseAsync(int id)
        {
            return await _context.Franchises.FindAsync(id);
        }
        
        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises.ToListAsync();
        }

        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            _context.SaveChangesAsync();
            return franchise;
        }

        public async Task AddMovieToFranchiseAsync(int id, List<int> movieIds)
        {
            Franchise franchiseToUpdateMovies = await _context.Franchises
                .Include(m => m.Movies)
                .Where(m => m.Id == id)
                .FirstAsync();
            List<Movie> movies = new List<Movie>();
            foreach (int movId in movieIds)
            {
                Movie movie = await _context.Movies.FindAsync(movId);
                if (movie == null)
                    throw new KeyNotFoundException();
                movies.Add(movie);
            }
            franchiseToUpdateMovies.Movies = movies;
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }

        public async Task<List<Movie>> GetMoviesInFranchiseAsync(int id)
        {
            var franchiseMovies = await _context.Movies.Where(m => m.FranchiseId == id).ToListAsync();
            return franchiseMovies;
        }

        public async Task<List<Character>> GetCharactersInFranchiseAsync(int id)
        {
            var franchiseMovies = await _context.Movies.Where(m => m.FranchiseId == id).ToListAsync();

            var characterInFranchise = await _context.Characters.Where(c => c.Movies.Any(x => franchiseMovies.Contains(x))).ToListAsync();
            return characterInFranchise;
        }

        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

    }
}
