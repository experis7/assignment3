﻿using Assignment3.Data;
using Assignment3.Model.Domain;
using Assignment3.Model.DTO.Character;
using Assignment3.Model.DTO.Movie;
using Assignment3.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net.Mime;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Assignment3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharacterController : ControllerBase
    {
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;

        public CharacterController(ICharacterService characterService, IMapper mapper)
        {
            _characterService = characterService;
            _mapper = mapper;
        }

        /// <summary>
        /// This method retrieves all characters from the database.
        /// </summary>
        /// <returns>List of all characters as DTO</returns>
        // GET: api/<CharacterController>
        [HttpGet]
        public async Task<IEnumerable<CharacterReadDTO>> GetAllCharactersAsync()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _characterService.GetAllCharactersAsync());
        }

        /// <summary>
        /// This method retrieves a character with the id from the parameter if it exists.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>CharacterReadDTO</returns>
        // GET api/<CharacterController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            Character character = await _characterService.GetSpecificCharacterAsync(id);
            if(character == null) 
            {
                return NotFound();
            }
            return _mapper.Map<CharacterReadDTO>(character);
        }
        /// <summary>
        /// This method adds a new character to the database.
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        // POST api/<CharacterController>
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CharacterCreateDTO dtoCharacter)
        {
            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
            domainCharacter = await _characterService.AddCharacterAsync(domainCharacter);
            return CreatedAtAction("GetCharacter", new { id = domainCharacter.Id }, _mapper.Map<CharacterReadDTO>(domainCharacter));

        }
        
        /// <summary>
        /// This method removes a character with the given Id from the database if it exists.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE api/<CharacterController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }

            await _characterService.DeleteCharacterAsync(id);

            return NoContent();

        }
        /// <summary>
        /// Updates a Character. Must pass a full character object and Id in route.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoCoach"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO characterEditDTO)
        {
            if (id != characterEditDTO.Id)
            {
                return BadRequest();
            }

            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }

            Character domainCharacter = _mapper.Map<Character>(characterEditDTO);
            await _characterService.UpdateCharacterAsync(domainCharacter);

            return NoContent();
        }

    }
}
