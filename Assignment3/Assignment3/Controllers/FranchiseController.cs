﻿using Assignment3.Data;
using Assignment3.Model.Domain;
using Assignment3.Model.DTO.Character;
using Assignment3.Model.DTO.Franchise;
using Assignment3.Model.DTO.Movie;
using Assignment3.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net.Mime;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Assignment3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class FranchiseController : ControllerBase
    {
        private readonly IFranchiseService _franchiseService;
        private readonly IMapper _mapper;

        public FranchiseController(IFranchiseService franchiseService, IMapper mapper)
        {
            _franchiseService = franchiseService;
            _mapper = mapper;
        }


        /// <summary>
        /// The method retrieves all franchises from the database.
        /// </summary>
        /// <returns>List of the data transfer object for reading franchises</returns>
        // GET: api/<FranchiseController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchisesAsync());
        }

        /// <summary>
        /// The method retrives a franchise with the id from the parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A data transfer object for reading franchises</returns>
        // GET api/<FranchiseController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _franchiseService.GetSpesificFranchiseAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }
            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// The method creates a new franchise in the database.
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        // POST api/<FranchiseController>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO franchise)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(franchise);
            domainFranchise = await _franchiseService.AddFranchiseAsync(domainFranchise);

            return CreatedAtAction("GetFranchise", new { id = domainFranchise.Id }, _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        /// <summary>
        /// Method for adding a movie to the franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieIds"></param>
        /// <returns></returns>
        /// <exception cref="KeyNotFoundException"></exception>
        // PUT api/<FranchiseController>/5/movies
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> PutMoviesToFranchise(int id, List<int> movieIds)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            try
            {
                await _franchiseService.AddMovieToFranchiseAsync(id, movieIds);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid movie.");
            }

            return NoContent();


        }
        /// <summary>
        /// Retrives a list of all the movies in a franchise from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A data transfer object for reading movies</returns>
        //GET api/<FranchiseController>/id/movies
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<List<MovieReadDTO>>> GetMoviesInFranchise(int id)
        {
            var franchiseMovies = await _franchiseService.GetMoviesInFranchiseAsync(id);
            if (franchiseMovies == null)
            {
                return NotFound();
            }
           
            return _mapper.Map<List<MovieReadDTO>>(franchiseMovies);
            
        }

        /// <summary>
        /// The method retrives all characters that exists in the movies in the franchise from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A data transfer object for reading characters</returns>
        //GET api/<FranchiseController>/id/movies/characters
        [HttpGet("{id}/movies/characters")]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetCharactersInFranchise(int id)
        {

            var characterInFranchise = await _franchiseService.GetCharactersInFranchiseAsync(id);
            if (characterInFranchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<CharacterReadDTO>>(characterInFranchise);

        }

        /// <summary>
        /// This method deletes a franchise if it exists with the given id from the parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE api/<FranchiseController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            
            if(!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            await _franchiseService.DeleteFranchiseAsync(id);
            return NoContent();
        }

        /// <summary>
        /// Updates a franchise. Must pass a full coach object and Id in route.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchiseEditDto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCoach(int id, FranchiseEditDTO franchiseEditDTO)
        {
            if (id != franchiseEditDTO.Id)
            {
                return BadRequest();
            }

            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(franchiseEditDTO);
            await _franchiseService.UpdateFranchiseAsync(domainFranchise);

            return NoContent();
        }

    }
}
