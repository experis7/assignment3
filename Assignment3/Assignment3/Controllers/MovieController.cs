﻿using Assignment3.Data;
using Assignment3.Model.Domain;
using Assignment3.Model.DTO.Character;
using Assignment3.Model.DTO.Movie;
using Assignment3.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net.Mime;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Assignment3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]

    public class MovieController : ControllerBase
    {
        private readonly IMovieService _movieService;
        private readonly IMapper _mapper;

        public MovieController(IMovieService movieService, IMapper mapper)
        {
            _movieService = movieService;
            _mapper = mapper;
        }

        /// <summary>
        /// This method retrieves all movies from the database.
        /// </summary>
        /// <returns>List of all movies as read DTO</returns>
        // GET: api/<MovieController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMoviesAsync());

        }
        /// <summary>
        /// This mehods retrives a movie with the id from the parameter if it exists.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A movie read DTO</returns>
        // GET api/<MovieController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _movieService.GetSpecificMovieAsync(id);
            if(movie == null)
            {
                return NotFound();
            }
            return _mapper.Map<MovieReadDTO>(movie);
            
        }

        /// <summary>
        /// Updates a movie. Must pass a full coach object and Id in route.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoCoach"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCoach(int id, MovieEditDTO dtoMovie)
        {
            if (id != dtoMovie.Id)
            {
                return BadRequest();
            }

            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);
            await _movieService.UpdateMovieAsync(domainMovie);

            return NoContent();
        }


        /// <summary>
        /// This method creates a new movie.
        /// </summary>
        /// <param name="movie"></param>
        /// <returns>The created movie</returns>
        // POST api/<MovieController>
        [HttpPost]
        public async Task<ActionResult<Movie>> Post(MovieCreateDTO movie)
        {
            var movieToAdd = _mapper.Map<Movie>(movie);
            movieToAdd = await _movieService.AddMovieAsync(movieToAdd);

            return CreatedAtAction("GetMovie", new { id = movieToAdd.Id }, _mapper.Map<MovieReadDTO>(movieToAdd));

        }

        
        /// <summary>
        /// This method adds characters in a movie with the id from the parameter if it exists. The caracters is passed in with an array of integers that will work as character Ids.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterIds"></param>
        /// <returns></returns>
        /// <exception cref="KeyNotFoundException"></exception>
        // PUT api/<MovieController>/5/characters
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> PutCharactersToMovies(int id, List<int> characterIds)
        {

            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            try
            {
                await _movieService.PutCharactersInMovieAsync(id, characterIds);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid certification.");
            }

            return NoContent();


        }
        /// <summary>
        /// This method retrieves all characters in a movie if there is any with the parameter id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterIds"></param>
        /// <returns>List of characters as DTOs that plays in the movie</returns>
        // GET api/<MovieController>/5/characters
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetCharactersInMovie(int id)
        {
            var characters = await _movieService.GetCharactersInMovieAsync(id);
            if (characters == null)
            {
                return NotFound();
            }




            return _mapper.Map<List<CharacterReadDTO>>(characters);

        }
        /// <summary>
        /// This method removes the movie with the given id from the database if it exists.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE api/<MovieController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            await _movieService.DeleteMovieAsync(id);

            return NoContent();

        }
    }
}
