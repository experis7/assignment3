﻿using System.ComponentModel.DataAnnotations;

namespace Assignment3.Model.Domain
{
    public class Character
    {

        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string FullName { get; set; }
        [StringLength(30)]
        public string? Alias { get; set; }
        [Required]
        [StringLength(10)]
        public string Gender { get; set; }
        public string? Picture { get; set; }
        public ICollection<Movie> Movies { get; set; }
    }
}
