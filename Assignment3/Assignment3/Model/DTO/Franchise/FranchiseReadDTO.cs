﻿namespace Assignment3.Model.DTO.Franchise
{
    public class FranchiseReadDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
